const bcrypt = require('bcrypt')

const userModel = require('../models/User')
const UserDto = require('../dtos/user')
const ApiError = require('../exceptions/api-error')

const tokenService = require('./token')

const SALT = 3

class User {
    async register(email, password) {
        const candidate = await userModel.findOne({ email })

        if (candidate) {
            throw ApiError.BadRequest(`Пользователь ${email} уже существует.`)
        }
        const hashPassword = await bcrypt.hash(password, SALT)
        const user = await userModel.create({ email, password: hashPassword })

        const userDto = new UserDto(user) // id, email
        const tokens = tokenService.generateToken({ ...userDto })
        await tokenService.saveToken(userDto.id, tokens.refreshToken)

        return {
            ...tokens,
            user: userDto
        }
    }

    async login(email, password) {
        const user = await userModel.findOne({ email })
        if (!user) {
            throw ApiError.BadRequest(`Пользователь ${email} не был найден.`)
        }

        const isPassEquals = await bcrypt.compare(password, user.password)

        if (!isPassEquals) {
            throw ApiError.BadRequest('Неверный пароль')
        }

        const userDto = new UserDto(user)
        const tokens = tokenService.generateToken({ ...userDto })
        await tokenService.saveToken(userDto.id, tokens.refreshToken)

        return {
            ...tokens,
            user: userDto
        }
    }

    async logout(refreshToken) {
        const token = await tokenService.removeToken(refreshToken)
        return token
    }

    async refresh(refreshToken) {
        if (!refreshToken) {
            throw ApiError.UnauthorizedError()
        }

        const userData = tokenService.validateRefreshToken(refreshToken)
        const tokenFromDb = tokenService.findToken(refreshToken)

        if (!userData || !tokenFromDb) {
            throw ApiError.UnauthorizedError()
        }

        const user = await userModel.findById(userData.id)
        const userDto = new UserDto(user)
        const tokens = tokenService.generateToken({ ...userDto })
        await tokenService.saveToken(userDto.id, tokens.refreshToken)

        return {
            ...tokens,
            user: userDto
        }
    }
}

module.exports = new User()
