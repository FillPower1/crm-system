const router = require('express').Router()

const controller = require('../controllers/position')

module.exports = router
    .get('/:id', controller.getByCategoryId)
    .post('/', controller.create)
    .patch('/:id', controller.update)
    .delete('/:id', controller.remove)
