const router = require('express').Router()

const controller = require('../controllers/analytics')

module.exports = router
    .get('/overview', controller.overview)
    .get('/analytics', controller.analytics)
