const router = require('express').Router()

const controller = require('../controllers/order')

module.exports = router
    .get('/', controller.getAll)
    .post('/', controller.create)
