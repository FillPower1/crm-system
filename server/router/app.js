const Router = require('express').Router
const { body } = require('express-validator')

const user = require('../controllers/user')
const authMiddleware = require('../middlewares/auth')

module.exports = Router()
    .all('/test', authMiddleware, (req, res) => {
        res.json([123,4])
    })
    .post('/register',
        body('email').isEmail(),
        body('password').isLength({ min: 4 }),
        user.register
    )
    .post('/login', user.login)
    .post('/logout', user.logout)
    .post('/refresh', user.refresh)
