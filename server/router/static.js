const router = require('express').Router()
const staticRoute = require('express').static
const compression = require('compression')

module.exports = router
    .use(compression())
    .use('/', staticRoute('dist'))
    .use('/fonts', staticRoute('dist/fonts'))
    .use('/favicons', staticRoute('dist/favicons'))
    .use('/robots.txt', staticRoute('dist/robots.txt'))
