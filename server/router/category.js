const router = require('express').Router()

const controller = require('../controllers/category')

module.exports = router
    .get('/', controller.getAll)
    .get('/:id', controller.getById)
    .delete('/:id', controller.remove)
    .post('/', controller.create)
    .patch('/:id', controller.update)
