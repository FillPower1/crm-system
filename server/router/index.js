const router = require('express').Router()
const passport = require('passport')

const authRoutes = require('./auth')
const analyticsRoutes = require('./analytics')
const categoryRoutes = require('./category')
const orderRoutes = require('./order')
const positionRoutes = require('./position')

router
    .use('/auth', authRoutes)
    .use('/analytics', analyticsRoutes)
    .use('/category', passport.authenticate('jwt', { session: false }), categoryRoutes)
    .use('/order', orderRoutes)
    .use('/position', positionRoutes)

module.exports = router
