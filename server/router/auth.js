const router = require('express').Router()

const controller = require('../controllers/auth')

module.exports = router
    .post('/login', controller.login)
    .post('/register', controller.register)
