require('dotenv').config({ path: 'dev.env' })

const express = require('express')
const { json, urlencoded } = require('express')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const morgan = require('morgan')
const passport = require('passport')

const router = require('./router')
const staticRoutes = require('./router/static')
const errorMiddleware = require('./middlewares/error-handler')

const server = express()

server
    .disable('x-powered-by')
    .use(passport.initialize())
    .use(morgan('dev'))
    .use(cors())
    .use(json())
    .use(urlencoded({ extended: true }))
    .use(cookieParser())
    .use('/api', router)
    .use(staticRoutes)
    .use(errorMiddleware)

require('./middlewares/passport')(passport)

module.exports = server
