const ApiError = require('../exceptions/api-error')
const tokenService = require('../services/token')

module.exports = (req, _, next) => {
    try {
        const authorizationHeader = req.headers.authorization || ''
        if (!authorizationHeader) {
            return next(ApiError.UnauthorizedError())
        }

        const accessToken = authorizationHeader.split(' ')[1]
        if (!accessToken) {
            return next(ApiError.UnauthorizedError())
        }

        const user = tokenService.validateAccessToken(accessToken)
        if (!user) {
            return next(ApiError.UnauthorizedError())
        }

        req.user = user
        next()
    } catch (error) {
        return next(ApiError.UnauthorizedError())
    }
}
