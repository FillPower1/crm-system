const ApiError = require('../exceptions/api-error')

module.exports = (error, req, res, next) => {
    // eslint-disable-next-line no-console
    console.log(error)

    if (error instanceof ApiError) {
        return res
            .status(error.status)
            .json({
                message: error.message,
                errors: error.errors
            })
    }

    return res.status(500).json({
        message: 'Что-то пошло не так...',
        errors: error.errors
    })
}
