/* eslint-disable no-console */
const mongoose = require('mongoose')

// eslint-disable-next-line no-magic-numbers
const PORT = process.env.PORT || 8080

async function connectToDataBase() {
    try {
        await mongoose.connect(process.env.DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        console.log('MongoDB connected')
    } catch (error) {
        console.log('Ошибка подключения к базе данных', error.message)
    }
}

module.exports = async server => {
    await connectToDataBase()
    server.listen(PORT, () => console.log(
        `Server running on: \n\t * http://localhost:${PORT} \n`
    ))
}
