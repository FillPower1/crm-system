const { validationResult } = require('express-validator')

const userService = require('../services/user')
const ApiError = require('../exceptions/api-error')

const REFRESH_TOCKEN = 'refreshToken'
const cookiesOptions = {
    // eslint-disable-next-line no-magic-numbers
    maxAge: 30 * 24 * 60 * 60 * 1000, // 30 дней
    httpOnly: true
}

class User {
    async register(req, res, next) {
        try {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return next(ApiError.BadRequest('Ошибка при валидации', errors.array()))
            }

            const { email, password } = req.body
            const userData = await userService.register(email, password)

            res.cookie(REFRESH_TOCKEN, userData.refreshToken, cookiesOptions)
            return res.json(userData)
        } catch (error) {
            next(error)
        }
    }

    async login(req, res, next) {
        try {
            const { email, password } = req.body
            const userData = await userService.login(email, password)
            res.cookie(REFRESH_TOCKEN, userData.refreshToken, cookiesOptions)

            return res.json(userData)
        } catch (error) {
            next(error)
        }
    }

    async logout(req, res, next) {
        try {
            const { refreshToken } = req.cookies
            const token = await userService.logout(refreshToken)
            res.clearCookie(REFRESH_TOCKEN)
            res.json(token)
        } catch (error) {
            next(error)
        }
    }

    async refresh(req, res, next) {
        try {
            const { refreshToken } = req.cookies
            const userData = await userService.refresh(refreshToken)
            res.cookie(REFRESH_TOCKEN, userData.refreshToken, cookiesOptions)

            return res.json(userData)
        } catch (error) {
            next(error)
        }
    }
}

module.exports = new User()
