const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const User = require('../models/User')

module.exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body
        const candidate = await User.findOne({ email })

        if (candidate) {
            const passwordResult = bcrypt.compareSync(password, candidate.password)
            if (passwordResult) {
                const token = jwt.sign({
                    email: candidate.email,
                    userId: candidate._id
                },
                process.env.JWT_ACCESS_SECRET,
                { expiresIn: '1h' }
                )

                res.status(200).json({
                    token,
                    email: candidate.email
                })
            } else {
                res.status(400).json({
                    message: 'Неправильный логин или пароль.'
                })
            }
        } else {
            res.status(404).json({
                message: 'Пользователь не найден.'
            })
        }
    } catch (error) {
        next(error)
    }
}

module.exports.register = async (req, res, next) => {
    try {
        const { email, password } = req.body
        const candidate = await User.findOne({ email })

        if (candidate) {
            res.status(400).json({
                message: 'Такой email уже занят.'
            })
        } else {
            const salt = bcrypt.genSaltSync()

            const user = new User({
                email,
                password: bcrypt.hashSync(password, salt)
            })
            await user.save()

            res.json(user)
        }
    } catch (error) {
        next(error)
    }
}
