import React from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { Container, Drawer, Button, Divider, makeStyles } from '@material-ui/core'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'

const useStyles = makeStyles({
    bar: {
        justifyContent: 'space-between'
    },
    icon: {
        padding: 0
    },
    list: {
        width: 300
    }
})

export const Navigation = ({ logout }) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const [toggleMenu, setToggleMenu] = React.useState(false)

    const toggleDrawer = React.useCallback(() => {
        setToggleMenu(!toggleMenu)
    }, [toggleMenu])

    const logoutHandler = React.useCallback(() => {
        dispatch(logout())
    }, [])

    return (
        <>
            <AppBar position="static">
                <Container maxWidth="lg">
                    <Toolbar className={classes.bar}>
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            onClick={toggleDrawer}
                            className={classes.icon}
                            size="medium"
                        >
                            <MenuIcon fontSize="large" />
                        </IconButton>
                        <Button color="inherit" onClick={logoutHandler}>
                            Выйти
                        </Button>
                    </Toolbar>
                </Container>
            </AppBar>
            <Drawer anchor="left" open={toggleMenu} onClose={toggleDrawer}>
                <div
                    role="presentation"
                    onClick={toggleDrawer}
                    onKeyDown={toggleDrawer}
                    className={classes.list}
                >
                    <List>
                        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                            <ListItem button key={text}>
                                <ListItemIcon>
                                    {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                                </ListItemIcon>
                                <ListItemText primary={text} />
                            </ListItem>
                        ))}
                    </List>
                    <Divider />
                    <List>
                        {['All mail', 'Trash', 'Spam'].map((text, index) => (
                            <ListItem button key={text}>
                                <ListItemIcon>
                                    {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                                </ListItemIcon>
                                <ListItemText primary={text} />
                            </ListItem>
                        ))}
                    </List>
                </div>
            </Drawer>
        </>
    )
}

Navigation.propTypes = {
    logout: PropTypes.func.isRequired
}
