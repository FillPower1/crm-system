import React from 'react'
import Typography from '@material-ui/core/Typography'

import style from './style.css'

export const NotFound = () => (
    <div className={style.wrapper}>
        <Typography variant="h2" gutterBottom>
            404. Ничего не найдено
        </Typography>
    </div>
)
