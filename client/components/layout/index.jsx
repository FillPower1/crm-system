import { Container } from '@material-ui/core'
import React from 'react'

import style from './style.css'

export const Layout = ({ children }) => (
    <div className={style.layout}>
        <Container maxWidth="lg">{children}</Container>
    </div>
)
