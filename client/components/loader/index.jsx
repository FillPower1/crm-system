import React from 'react'
import { CircularProgress } from '@material-ui/core'

import style from './style.css'

export const Loader = () => (
    <div className={style.wrapper}>
        <CircularProgress size={150} />
    </div>
)
