import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'
import PropTypes from 'prop-types'

import { logout } from '../../store/actions/user'
import { isAuthorized } from '../../store/selectors/user'
import { Navigation } from '../navigation'
import { Layout } from '../layout'

export const PrivateRoute = ({ component: Component, ...rest }) => {
    const auth = useSelector(isAuthorized)

    if (!auth) {
        return (
            <Route
                {...rest}
                render={({ location }) => (
                    <Redirect
                        to={{
                            pathname: '/',
                            state: { from: location }
                        }}
                    />
                )}
            />
        )
    }

    return (
        <Route {...rest}>
            <Navigation logout={logout} />
            <Layout>
                <Component />
            </Layout>
        </Route>
    )
}

PrivateRoute.propTypes = {
    component: PropTypes.elementType
}
