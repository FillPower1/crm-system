import axios, { originalAxios, ACCESS_TOKEN } from '../axios'
import * as types from '../action-types'

const sleep = (ms = 0) => new Promise(resolve => setTimeout(resolve, ms))

export const init = () => async dispatch => {
    try {
        const accessToken = localStorage.getItem(ACCESS_TOKEN)
        if (!accessToken) {
            return
        }

        dispatch({
            type: types.LOGIN_REQUEST
        })

        const { data } = await originalAxios.post('/api/refresh', {}, {
            withCredentials: true
        })
        localStorage.setItem(ACCESS_TOKEN, data?.accessToken)

        dispatch({
            type: types.LOGIN_SUCCESS
        })
    } catch (error) {
        dispatch({
            type: types.LOGIN_ERROR
        })
    }
}

export const login = ({ email, password }) => async dispatch => {
    try {
        dispatch({
            type: types.LOGIN_REQUEST
        })

        const { data } = await axios.post('/login', { email, password })
        localStorage.setItem(ACCESS_TOKEN, data?.accessToken)

        dispatch({
            type: types.LOGIN_SUCCESS
        })
    } catch (error) {
        localStorage.removeItem(ACCESS_TOKEN)
        dispatch({
            type: types.LOGIN_ERROR
        })
        // await sleep();
        alert(error.response.data?.message || 'Произошла ошибка')
    }
}

export const register = ({ email, password }) => async dispatch => {
    try {
        dispatch({
            type: types.REGISTER_REQUEST
        })

        const { data } = await axios.post('/register', { email, password })
        localStorage.setItem(ACCESS_TOKEN, data?.accessToken)

        dispatch({
            type: types.REGISTER_SUCCESS
        })
    } catch (error) {
        dispatch({
            type: types.REGISTER_ERROR
        })
    }
}

export const logout = () => async dispatch => {
    try {
        await axios.post('/logout')
        localStorage.removeItem(ACCESS_TOKEN)

        dispatch({
            type: types.LOGOUT_SUCCESS
        })
    } catch (error) {
        dispatch({
            type: types.LOGIN_ERROR
        })
    }
}
