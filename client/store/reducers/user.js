import * as types from '../action-types'

const initialState = {
    firstName: '',
    email: '',
    isLoading: false,
    isAuthorized: false,
    hasError: false
}
// TODO: refactor
export default (state = initialState, action) => {
    switch (action.type) {
    case types.LOGIN_REQUEST:
    case types.REGISTER_REQUEST:
        return {
            ...state,
            isLoading: true
        }
    case types.LOGIN_SUCCESS:
    case types.REGISTER_SUCCESS:
        return {
            ...state,
            isLoading: false,
            isAuthorized: true
        }
    case types.LOGIN_ERROR:
    case types.REGISTER_ERROR:
        return {
            ...state,
            isLoading: false,
            isAuthorized: false,
            hasError: true
        }
    case types.LOGOUT_SUCCESS:
        return {
            ...state,
            isAuthorized: false,
        }
    default:
        return state
    }
}
