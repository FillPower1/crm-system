export const isAuthorized = state => state.user.isAuthorized
export const isLoading = state => state.user.isLoading
export const errorMessage = state => state.user.errorMessage
export const hasError = state => state.user.hasError
