import axios from 'axios'

export const ACCESS_TOKEN = 'accessToken'

const axiosInstance = axios.create({
    withCredentials: true,
    baseURL: '/api'
})

axiosInstance.interceptors.request.use(config => {
    config.headers.authorization = `Bearer ${localStorage.getItem(ACCESS_TOKEN) ?? ''}`
    return config
})

axiosInstance.interceptors.response.use(
    config => {
        return config
    },
    async error => {
        const originalRequest = error.config

        // eslint-disable-next-line no-magic-numbers
        if (error.response.status === 401 && !error?.config?._isFetched) {
            originalRequest._isFetched = true
            try {
                const { data } = await axios.post('/api/refresh')
                localStorage.setItem(ACCESS_TOKEN, data?.accessToken)
                return axiosInstance.request(originalRequest)
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(`Refetching failed in ${originalRequest}`)
            }
        }

        throw error
    }
)

export { axios as originalAxios }
export default axiosInstance
