import { createStore as createReduxStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { devToolsEnhancer } from 'redux-devtools-extension'

import * as reducers from './reducers'

const createStore = () => {
    const reducer = combineReducers({
        ...reducers
    })

    const composedEnhancer = compose(
        applyMiddleware(thunkMiddleware),
        devToolsEnhancer({
            name: 'crm-system',
            serialize: true
        })
    )

    return createReduxStore(reducer, composedEnhancer)
}

export default createStore()
