import React from 'react'
import { Redirect } from 'react-router-dom'
import { Button, TextField, Container, makeStyles } from '@material-ui/core'

import { useLogin } from './use-login'
import style from './style.css'

const useStyles = makeStyles({
    input: {
        marginBottom: 32
    }
})

export const Login = () => {
    const classes = useStyles()
    const { formik, isLoggedIn } = useLogin()

    if (isLoggedIn) {
        return <Redirect to="/dashboard" />
    }

    return (
        <Container maxWidth="lg">
            <div className={style.wrapper}>
                <form className={style.form} onSubmit={formik.handleSubmit}>
                    <TextField
                        fullWidth
                        id="email"
                        name="email"
                        label="Email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                        className={classes.input}
                    />
                    <TextField
                        fullWidth
                        id="password"
                        name="password"
                        label="Пароль"
                        type="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        error={formik.touched.password && Boolean(formik.errors.password)}
                        helperText={formik.touched.password && formik.errors.password}
                        className={classes.input}
                    />
                    <Button color="primary" variant="contained" fullWidth type="submit">
                        Войти
                    </Button>
                </form>
            </div>
        </Container>
    )
}
