import { useSelector, useDispatch } from 'react-redux'
import { useFormik } from 'formik'
import * as yup from 'yup'

import { login } from '../../store/actions/user'
import { isAuthorized, hasError } from '../../store/selectors/user'

const validationSchema = yup.object({
    email: yup
        .string('Введите email')
        .email('Введите корректный email')
        .required('Email обязателен'),
    password: yup
        .string('Введите пароль')
        .min(6, 'Минимум 6 символов')
        .required('Пароль обязателен')
})

export function useLogin() {
    const dispatch = useDispatch()
    const isLoggedIn = useSelector(isAuthorized)
    const error = useSelector(hasError)

    const formik = useFormik({
        initialValues: { email: 'test@mail.ru', password: '1234561' },
        validationSchema,
        onSubmit: values => {
            dispatch(login(values))
        }
    })

    return {
        isLoggedIn,
        error,
        formik
    }
}
