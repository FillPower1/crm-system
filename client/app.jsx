import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BrowserRouter as ReactRouter, Switch, Route } from 'react-router-dom'

import { isLoading as isFetching } from './store/selectors/user'
import { init } from './store/actions/user'
import { Loader } from './components/loader'
import { PrivateRoute } from './components/private-route.jsx/index.jsx'
import { Login } from './pages/login'
import { NotFound } from './components/not-found'
import { Dashboard } from './pages/dashboard'

const Router = () => (
    <ReactRouter>
        <Switch>
            <Route exact path="/" component={Login} />
            <PrivateRoute exact path="/dashboard" component={Dashboard} />
            <Route path="*" component={NotFound} />
        </Switch>
    </ReactRouter>
)

export const App = () => {
    const dispatch = useDispatch()
    const isLoading = useSelector(isFetching)

    React.useEffect(() => {
        dispatch(init())
    }, [])

    if (isLoading) {
        return <Loader />
    }

    return (
        <Router />
    )
}
