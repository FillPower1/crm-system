# CRM-System
> CRM система для отчета и автоматизации рутийных работ.

## Установка

    git clone https://gitlab.com/FillPower1/crm-system.git
    cd crm-system
    npm install

## Запуск

    npm start - dev server для локальной разработки
    docker:start - сборка и запуск докер контейнеров
    docker:build - ребилд докер образов
    docker:stop - остановка всех контейнеров

### Приложение доступно по адресу

    Клиент:
    http://localhost:4000
    
    Сервер:
    https://localhost:5000

    
## Сборка для production

    npm run build

## Языки & инструменты

### JavaScript

-   [React](http://facebook.github.io/react) используется для пользовательского интерфейса
-   [Redux](https://redux.js.org//) используется для управления состоянием приложения
-   [Postcss](https://github.com/postcss/postcss) используется для стилизации компонентов
-   [Material UI](https://material-ui.com/ru/) в качестве основного UI Kit
