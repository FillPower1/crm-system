FROM ubuntu:latest

RUN apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash \
    && apt-get install nodejs -yq && apt install -y netcat

WORKDIR /var/www/

COPY ./ /var/www/
COPY ./utils/wait-for.sh wait-for.sh
RUN chmod +x wait-for.sh
RUN npm install

EXPOSE 8080

RUN npm run build