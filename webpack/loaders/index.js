const assets = require('./assets')
const css = require('./css')
const javascript = require('./javascript')

module.exports = {
    assets,
    css,
    javascript
}
