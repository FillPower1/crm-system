const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const { ENVS } = require('../env')

const loader = ENVS.__DEV__ ? [
    'style-loader',
    {
        loader: 'css-loader',
        options: {
            modules: {
                localIdentName: '[path][name]__[local]--[hash:base64:5]',
                exportLocalsConvention: 'camelCase',
                exportOnlyLocals: false
            },
            sourceMap: true
        }
    },
    {
        loader: 'postcss-loader',
        options: {
            sourceMap: true
        }
    }
] : [
    MiniCssExtractPlugin.loader,
    {
        loader: 'css-loader',
        options: {
            modules: {
                localIdentName: '[hash:base64:8]',
                exportLocalsConvention: 'camelCase',
                exportOnlyLocals: false
            },
            sourceMap: false
        }
    },
    {
        loader: 'postcss-loader',
        options: {
            sourceMap: false
        }
    }
]

module.exports = {
    test: /\.css$/,
    use: loader
}
