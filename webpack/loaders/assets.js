module.exports = {
    test: /^(?!.*\.inline).*\.(jpe?g|png|gif)$/,
    use: [
        {
            loader: 'file-loader',
            options: {
                name: 'assets/[name]-[hash:5].[ext]'
            }
        }
    ]
}
