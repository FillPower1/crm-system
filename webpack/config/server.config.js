const nodeExternals = require('webpack-node-externals')

const { DIST_DIR, SERVER_DIR } = require('../dir')
const { ENVS } = require('../env')

const { __PROD__ } = ENVS

module.exports = {
    name: 'server',
    target: 'node',
    entry: SERVER_DIR,
    output: {
        path: DIST_DIR,
        filename: 'server.js',
        libraryTarget: 'commonjs2'
    },
    resolve: {
        modules: ['node_modules'],
        extensions: ['*', '.js', '.json'],
    },
    externals: [nodeExternals({ allowlist: [/\.(?!(?:jsx?|json)$).{1,5}$/i] })],
    performance: {
        hints: false
    },
    optimization: {
        minimize: __PROD__
    }
}
