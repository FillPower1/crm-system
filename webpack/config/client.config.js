const path = require('path')

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require('copy-webpack-plugin')

const { DIST_DIR, CLIENT_DIR } = require('../dir')
const { GLOBAL_ARGS, env, ENVS } = require('../env')
const { javascript, assets, css } = require('../loaders')
const devServer = require('../dev-server')

const { __DEV__, __PROD__ } = ENVS

module.exports = {
    target: 'web',
    entry: path.resolve(CLIENT_DIR, 'index.jsx'),
    output: {
        path: DIST_DIR,
        publicPath: '/',
        filename: __DEV__ ? '[name].js' : '[name].[contenthash].js',
        hashDigestLength: 8
    },
    mode: env,
    module: {
        rules: [javascript, assets, css]
    },
    devServer,
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['*', '.js', '.jsx', '.json']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'static/index.html'
        }),
        new CopyPlugin({
            patterns: [
                { from: 'static/normalize.min.css', to: '.' },
            ]
        }),
        new webpack.DefinePlugin(GLOBAL_ARGS),
        __DEV__ && new webpack.HotModuleReplacementPlugin(),
        __PROD__ && new CleanWebpackPlugin(),
        __PROD__ && new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css'
        })
    ].filter(Boolean),
    stats: {
        all: undefined,
        builtAt: !__DEV__,
        chunks: !__DEV__,
        assets: !__DEV__,
        errors: true,
        warnings: true,
        outputPath: true,
        timings: true,
    },
    performance: {
        hints: env === 'production' ? 'warning' : false
    },
    devtool: env === 'production' ? false : 'source-map'
}
