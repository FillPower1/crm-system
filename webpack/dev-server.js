const path = require('path')

const { ENVS } = require('./env')

module.exports = ENVS.__DEV__ ? {
    contentBase: path.join(process.cwd(), 'dist'),
    port: 5000,
    hot: true,
    historyApiFallback: true,
    proxy: {
        '/api/*': {
            target: 'http://localhost:8080/',
            secure: false,
            changeOrigin: true
        }
    }
} : {}
