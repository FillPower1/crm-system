const env = process.env.NODE_ENV || 'development'

const ENVS = {
    __DEV__: env === 'development',
    __PROD__: env === 'production',
    __TEST__: env === 'testing'
}

const DEFAULT_PORT = 4000

const GLOBAL_ARGS = {
    ...ENVS,
    'process.env': {
        ...ENVS,
        NODE_ENV: JSON.stringify(env),
        PORT: process.env.PORT || DEFAULT_PORT
    }
}

module.exports = {
    env,
    ENVS,
    GLOBAL_ARGS
}
